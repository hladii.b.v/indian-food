<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'indian-food' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*Kz`<eNdSss#IC^03!;MS11p_rUw[yxJ;ED)o!6I,- SJ9).[K-Ux8b9&`T{F?7y' );
define( 'SECURE_AUTH_KEY',  '_l|j,RE&AQpC^g:_5v qRLa~Bq(rVm2t$.DSxi@S*}ncuthEM6hB9W@5pdAwb3Z-' );
define( 'LOGGED_IN_KEY',    '60_X_Nx06mM+FNJfhD{L84g+ub &*=Czk4T;YH1vh2)fbo=ZeUeoWI,Rj4*:fKLm' );
define( 'NONCE_KEY',        '<Eg|-e5Eri:kBk%YXO<uds`fiXZ+)Y7DCG:$P)9<NjQBmJZ#b))dt44T&I{gyL;b' );
define( 'AUTH_SALT',        'Oh`0c9y|G7KKAfJb#?QI1xH~}{%k63M`3nI!jY^B#=gB]!obdph@vBI!_?p}|etq' );
define( 'SECURE_AUTH_SALT', '[!2}L`/qTOp$iO=g#4B>n2L}Eu}Y7zLG(ddW{U9mAl#r,pZEW[[Qj<mSJug+/hWa' );
define( 'LOGGED_IN_SALT',   'L{]vlq5S5I,IAmXh;z<3[nK,]wc$}k$0@(;)bhf3ayfEs^4)0=w+8lW$/{@Uk?jC' );
define( 'NONCE_SALT',       '6$iH];3%HP{>x`v~Uj*lw@FEQCJFC9%wRioEsh)&A!k;i|oQ_2|Cl^W_}^I(d>i.' );
define('JWT_AUTH_SECRET_KEY', '#hZ([&_=+3>3h%+~?.@a9i4eNuq+M4Y.iy,Q>R)biw!b>Ay%r_t!eW+|qU2~5OvK');
define('JWT_AUTH_CORS_ENABLE', true);



/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'sh';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
